import Auth0Lock from "auth0-lock";

export default class Auth {
  lock = new Auth0Lock(
    process.env.REACT_APP_AUTH0_CLIENTID,
    process.env.REACT_APP_AUTH0_DOMAIN,
    {
      auth: {
        params: {
          scope: "openid profile email"
        }
      }
    }
  );
  token = null;

  setSession(accessToken) {
    this.token = accessToken;
    localStorage.setItem("token", accessToken);
  }

  isAuthenticated() {
    this.token = localStorage.getItem("token");
    return !!this.token;
  }

  onGetProfile(cb) {
    if (this.isAuthenticated()) {
      let accessToken = this.token;
      this.lock.getUserInfo(accessToken, (error, profile) => {
        if (error) {
          console.error(error);
        }
        return cb(accessToken, profile);
      });
    }
    this.lock.on("authenticated", ({ accessToken }) => {
      this.lock.getUserInfo(accessToken, (error, profile) => {
        if (error) {
          console.error(error);
        }
        this.setSession(accessToken);
        return cb(accessToken, profile);
      });
    });
  }

  login() {
    this.lock.show();
  }

  logout() {
    this.token = null;
    localStorage.removeItem("token");
    // this.lock.logout();
  }
}
