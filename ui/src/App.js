import React from "react";
import "semantic-ui-css/semantic.min.css";
import { BrowserRouter as Router, Switch } from "react-router-dom";
import AppProvider from "./client/Provider";
import { Base, Main, About, Profile } from "./pages";
import { menuLinksList } from "./config";

export default function App() {
  return (
    <AppProvider>
      <Router>
        <Switch>
          <Base exact path="/" component={Main} />
          {menuLinksList.map((pageCaption, key) => (
            <Base
              key={key}
              exact
              path={`/${pageCaption.url}`}
              pageTitle={pageCaption.name}
              component={About}
            />
          ))}
          <Base exact path="/profile" component={Profile} />
        </Switch>
      </Router>
    </AppProvider>
  );
}
