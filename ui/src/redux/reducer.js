import { combineReducers } from "redux";
import user from "./modules/user";
import page from "./modules/page";
export default combineReducers({
  user,
  page
});
