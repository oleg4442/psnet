import { take, put } from "redux-saga/effects";
import { LOGIN_SUCCESS, LOGIN, LOGOUT, loginSuccess } from "./modules/user";
import { isLoaded } from "./modules/page";
import Auth from "../auth";

const auth = new Auth();

function* handleLoginSuccess() {
  let resolve;
  auth.onGetProfile((token, profile) => resolve({ token, profile }));
  while (1) {
    // eslint-disable-next-line no-loop-func
    let loginPromise = new Promise(ok => (resolve = ok));
    const { token, profile } = yield loginPromise;
    yield put(loginSuccess(token, profile));
  }
}

function* handleLoading() {
  if (auth.isAuthenticated()) {
    yield take(LOGIN_SUCCESS);
    yield put(isLoaded());
  } else {
    yield put(isLoaded());
  }
}

function* handleLogin() {
  while (1) {
    yield take(LOGIN);
    auth.login();
  }
}

function* handleLogout() {
  while (1) {
    yield take(LOGOUT);
    auth.logout();
  }
}

export default [handleLoginSuccess, handleLogin, handleLogout, handleLoading];
