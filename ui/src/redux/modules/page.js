export const IS_LOADED = "psnet/user/IS_LOADED";
export const LOADING = "psnet/user/LOADING";

const initialState = {
  loaded: false
};
const makeReducer = actions => (state = initialState, action) => {
  if (!actions[action.type]) {
    return state;
  }
  return actions[action.type](state, action);
};
const actions = {
  [IS_LOADED](state) {
    return { ...state, loaded: true };
  },
  [LOADING](state) {
    return { loaded: false };
  }
};
export default makeReducer(actions);
export const isLoaded = () => ({
  type: IS_LOADED
});
export const loading = () => ({
  type: LOADING
});
