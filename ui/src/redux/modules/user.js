export const LOGIN = "psnet/user/LOGIN";
export const LOGOUT = "psnet/user/LOGOUT";
export const LOGIN_SUCCESS = "psnet/user/LOGIN_SUCCESS";

const initialState = {
  token: null,
  profile: null
};
const makeReducer = actions => (state = initialState, action) => {
  if (!actions[action.type]) {
    return state;
  }
  return actions[action.type](state, action);
};
const actions = {
  [LOGIN_SUCCESS](state, { token, profile }) {
    return {
      token,
      profile
    };
  },
  [LOGOUT](state) {
    return initialState;
  }
};
export default makeReducer(actions);
export const login = () => ({
  type: LOGIN
});
export const logout = () => ({
  type: LOGOUT
});
export const loginSuccess = (token, profile) => ({
  type: LOGIN_SUCCESS,
  token,
  profile
});
