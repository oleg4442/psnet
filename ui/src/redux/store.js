import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import createLogger from "redux-logger";

import reducer from "./reducer";
import sagas from "./sagas";

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
  reducer,
  applyMiddleware(sagaMiddleware, createLogger)
);

sagas.forEach(saga => sagaMiddleware.run(saga));

export default store;
