import Base from "./Base";
import Main from "./Main";
import About from "./About";
import Profile from "./Profile";

export { Base, Main, About, Profile };
