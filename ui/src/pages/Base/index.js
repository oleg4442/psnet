import React from "react";
import { Route } from "react-router-dom";
import BaseMenu from "../../components/menu";
import Footer from "../../components/footer";
import { menuLinksList } from "../../config";

const Base = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (
      <div
        style={{ display: "flex", minHeight: "100vh", flexDirection: "column" }}
      >
        {rest.path !== "/" && (
          <BaseMenu
            menuLinks={menuLinksList}
            stylesList={{ menu: { size: "large" } }}
          />
        )}
        <div style={{ flex: 1 }}>
          <Component {...props} pageTitle={rest.pageTitle || ""} />
        </div>
        <Footer />
      </div>
    )}
  />
);

export default Base;
