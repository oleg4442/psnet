import React, { Component } from "react";
import { Grid } from "semantic-ui-react";

class About extends Component {
  render() {
    return (
      <Grid padded>
        <Grid.Column color="purple">
          You are in {this.props.pageTitle}
        </Grid.Column>
      </Grid>
    );
  }
}

export default About;
