import React from "react";
import { Route } from "react-router-dom";
import { BaseMenu } from "../../components/menu";
import Footer from "../../components/footer";

const menuLinksList = [
  { name: "About us", url: "about" },
  { name: "News", url: "news" },
  { name: "Dog's stories", url: "stories" },
  { name: "Testimonails", url: "testimonails" },
  { name: "Contact us", url: "contact" },
  { name: "Social", url: "social" },
  { name: "Usefull info", url: "info" }
];

const Test = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (
      <div>
        <BaseMenu
          menuLinks={menuLinksList}
          stylesList={{ menu: { fixed: "top", size: "large" } }}
        />
        <Component {...props} />
        <Footer />
      </div>
    )}
  />
);

export default Test;
