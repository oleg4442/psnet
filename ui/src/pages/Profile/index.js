import React, { Component } from "react";
import { Card, Icon, Image, Grid, Container } from "semantic-ui-react";

class Profile extends Component {
  render() {
    return (
      <Container>
        <Grid>
          <Grid.Column width={4}>
            <Card>
              <Image src="http://localhost:3000/matthew.png" />
              <Card.Content>
                <Card.Header>Matthew</Card.Header>
                <Card.Meta>
                  <span className="date">Joined in 2015</span>
                </Card.Meta>
                <Card.Description>
                  Matthew is a musician living in Nashville.
                </Card.Description>
              </Card.Content>
              <Card.Content extra>
                <a>
                  <Icon name="user" />
                  22 Friends
                </a>
              </Card.Content>
            </Card>
          </Grid.Column>
          <Grid.Column width={9} />
          <Grid.Column width={3}>
            <Image src="/assets/images/wireframe/media-paragraph.png" />
          </Grid.Column>
        </Grid>
      </Container>
    );
  }
}

export default Profile;
