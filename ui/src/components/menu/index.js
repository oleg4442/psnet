import React from "react";
import PropTypes from "prop-types";
import { Container, Menu, Button } from "semantic-ui-react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { login, logout } from "../../redux/modules/user";
const { array, object, func, bool } = PropTypes;

const BaseMenu = ({
  menuLinks,
  stylesList,
  renderRight,
  profile,
  isLoading,
  doLogout,
  doLogin
}) => {
  return (
    <Menu {...stylesList.menu}>
      <Container>
        <Menu.Item as={Link} to="/">
          PSNet
        </Menu.Item>

        {menuLinks.map((link, key) => (
          <Menu.Item key={key} as={Link} to={`/${link.url}`}>
            {link.name}
          </Menu.Item>
        ))}
        {!!profile &&
          isLoading && (
            <Menu.Menu position="right">
              <Menu.Item className="item">{profile.email}</Menu.Item>
              <Menu.Item>
                <Button onClick={doLogout} as="a" primary>
                  Log out
                </Button>
              </Menu.Item>
            </Menu.Menu>
          )}
        {!profile &&
          isLoading && (
            <Menu.Menu position="right">
              <Menu.Item>
                <Button onClick={doLogin} as="a" primary>
                  Log in
                </Button>
              </Menu.Item>
            </Menu.Menu>
          )}
        {!!renderRight && renderRight()}
      </Container>
    </Menu>
  );
};

BaseMenu.propTypes = {
  menuLinks: array,
  stylesList: object,
  renderRight: func,
  doLogout: func,
  doLogin: func,
  isLoading: bool,
  profile: object
};

export default connect(
  state => ({ profile: state.user.profile, isLoading: state.page.loaded }),
  {
    doLogout: logout,
    doLogin: login
  }
)(BaseMenu);
