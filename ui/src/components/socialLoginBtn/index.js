import React from "react";
import PropTypes from "prop-types";
import { Button, Icon } from "semantic-ui-react";
import { connect } from "react-redux";
import { login } from "../../redux/modules/user";
const { func, string } = PropTypes;

const SLoginBtn = ({ onLogin, text, iconName }) => {
  return (
    <Button onClick={onLogin} primary size="huge">
      {text}
      <Icon name={iconName} style={{ marginLeft: 10, marginRight: 0 }} />
    </Button>
  );
};

SLoginBtn.propTypes = {
  onLogout: func,
  text: string,
  iconName: string
};

export default connect(
  state => ({
    user: state.user.profile
  }),
  { onLogin: login }
)(SLoginBtn);
