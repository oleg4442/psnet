export const menuLinksList = [
  { name: "About us", url: "about" },
  { name: "News", url: "news" },
  { name: "Dog's stories", url: "stories" },
  { name: "Testimonails", url: "testimonails" },
  { name: "Contact us", url: "contact" },
  { name: "Social", url: "social" },
  { name: "Usefull info", url: "info" }
];
