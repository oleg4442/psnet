import React from "react";
import { ApolloProvider } from "react-apollo";
import store from "../redux/store";
import client from "./";

export default function AppProvider({ children }) {
  return (
    <ApolloProvider store={store} client={client}>
      <div>{children}</div>
    </ApolloProvider>
  );
}
