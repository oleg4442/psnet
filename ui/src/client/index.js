import { ApolloClient, createNetworkInterface } from "apollo-client";
// import { SubscriptionClient, addGraphQLSubscriptions } from 'subscriptions-transport-ws';

// const wsClient = new SubscriptionClient('ws://localhost:4010/subscriptions');

const baseNetworkInterface = createNetworkInterface({
  uri: "http://localhost:8080/graphql",
  opts: {
    mode: "no-cors"
  }
});

// const subscriptionNetworkInterface = addGraphQLSubscriptions(
//   baseNetworkInterface
// );
const client = new ApolloClient({
  networkInterface: baseNetworkInterface
});

export default client;
