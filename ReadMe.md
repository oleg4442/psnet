1. Установить jdk 8 c сайта http://www.oracle.com/technetwork/java/javase/downloads/index.html
2. Скачать бинарник Maven https://maven.apache.org/download.cgi
Распаковать например в С:/Program Files/Maven 
Зайти в переменные окружения
Мой Компьютер - Свойства - Дополнительные параметры системы - Переменные среды - 
Создать две системные переменные 
M2_HOME С:\Program Files\Maven
MAVEN_HOME C:\Program Files\Maven
Добавить к переменной PATH %M2_HOME%\bin
3. Открыть консоль и убедиться что установлены java и maven
java -version
должна вывестись версия java
mvn -v
должна вывестись версия maven

4. Качаем проект серверную часть
git clone https://oleg4442@bitbucket.org/oleg4442/psnet.git

5. Заходим в папку проекта, где лежит pom.xml и запускаем

mvn jetty:run

Должно вывестись сообщение, что jetty сервер запущен

Проверяем ссылку http://localhost:8080/graphql?query={allLinks{url,description}}

