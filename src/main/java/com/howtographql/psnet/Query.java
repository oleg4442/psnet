import com.coxautodev.graphql.tools.GraphQLRootResolver;

import java.util.List;

public class Query implements GraphQLRootResolver {
    
    private final LinkRepository linkRepository;
	private final CatRepository catRepository;

    public Query(LinkRepository linkRepository, CatRepository catRepository) {
        this.linkRepository = linkRepository;
		this.catRepository = catRepository;
    }

    public List<Link> allLinks() {
        return linkRepository.getAllLinks();
    }
	
	public List<Cat> allCats(){
		return catRepository.getAllCats();
	}
}