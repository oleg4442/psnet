import java.util.List;
import java.util.ArrayList;

public class CatRepository {
    
    private final List<Cat> cats;

    public CatRepository() {
        cats = new ArrayList<>();
		cats.add(new Cat("Buff", "Mean cat"));
		cats.add(new Cat("Milka", "Nice cat girl"));
    }

    public List<Cat> getAllCats() {
        return cats;
    }
}